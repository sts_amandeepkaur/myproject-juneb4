from django.contrib import admin
from myapp.models import Student,Product,category,registration

admin.site.site_header = "Myproject || Site Name"

class StudentAdmin(admin.ModelAdmin):
    # fields = ['subtitle','name','reg_no','email']
    list_display = ['id','name','email','approved','reg_no','registred_on']
    list_filter = ['name','reg_no']
    list_editable = ['name','email']
    search_fields = ['name','reg_no']

admin.site.register(Student,StudentAdmin)
admin.site.register(Product)
admin.site.register(category)
admin.site.register(registration)
