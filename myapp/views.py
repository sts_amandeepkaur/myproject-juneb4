from django.shortcuts import render
from myapp.models import Product,category,registration
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.decorators import login_required

all_cat = category.objects.all().order_by('c_name')

def indexpage(request):
    all = Product.objects.all().order_by('-id')
    if request.method=="POST":
        s = request.POST['sr']
        all = Product.objects.filter(name__contains=s)
        return render(request,'index.html',{'p':all,'total':len(all),'cats':all_cat})

    if 'p_id' in request.GET:
        i = request.GET['p_id']
        all = Product.objects.filter(cat__id=i)
        return render(request,'index.html',{'p':all,'total':len(all),'cats':all_cat})  
    if 'del' in request.GET:
        did = request.GET['del']
        pp = Product.objects.get(id=did)
        pp.delete()
        return HttpResponseRedirect('/')
    return render(request,'index.html',{'p':all,'total':len(all),'cats':all_cat})

def aboutpage(request):
    if request.method=="POST":
        pn = request.POST['pname']
        mp = request.POST['mprice']
        sp = request.POST['oprice']
        cat_id = request.POST['catg']
        des = request.POST['desc']
        c_obj = category.objects.get(id=cat_id)
        if "add" in request.POST:
            data = Product(name=pn,cat=c_obj,m_price=mp,s_price=sp,description=des)
            data.save()
            if 'pimage' in request.FILES:
                im = request.FILES['pimage']
                data.image=im
                data.save()
            msz = "{} added Successfully".format(data.name)
            return render(request,'about.html',{'cats':all_cat,'rs':msz})
        if "update" in request.POST:
            id = request.GET['up']
            prd = Product.objects.get(id=id)
            prd.name=pn
            prd.cat = c_obj
            prd.m_price=mp
            prd.sale_price=sp
            prd.description=des
            prd.save()
            if 'pimage' in request.FILES:
                im = request.FILES['pimage']
                prd.image=im
                prd.save()
            msz = "{} Updated Successfully".format(prd.name)
            return render(request,'about.html',{'cats':all_cat,'rs':msz})
    if "up" in request.GET:
        uid = request.GET['up']
        prd = Product.objects.get(id=uid)
        return render(request,'about.html',{'cats':all_cat,'p':prd})
    return render(request,'about.html',{'cats':all_cat})
from django.contrib.auth.models import User
def signup(request):
    if request.method=="POST":
        fst = request.POST['first']
        lst = request.POST['last']
        un = request.POST['un']
        email = request.POST['em']
        pwd = request.POST['pas']
        cn = request.POST['con']
        usr = User.objects.create_user(un,email,pwd)
        usr.first_name = fst
        usr.last_name = lst
        usr.is_staff = True
        usr.save()

        rg = registration(user=usr,contact=cn)
        rg.save()
        if 'pic' in request.FILES:
            pic = request.FILES['pic']
            rg.profile_pic = pic
            rg.save()
        msz = "{} registred successfully!!".format(usr.first_name)
        return render(request,'signup.html',{'result':msz})
    return render(request,'signup.html')

def checkUserExist(r):
    if r.method == "GET":
        un = r.GET['usern']
        chk = User.objects.filter(username=un)
        if len(chk)>0:
            return HttpResponse(1)
        else:
            return HttpResponse(0)

def signin(request):
    if 'userid' in request.COOKIES:
        uid = request.COOKIES['userid']
        usr = User.objects.get(id=uid)
        login(request,usr)
        return HttpResponseRedirect('/dashboard')
    if request.method=="POST":
        un = request.POST['usern']
        pwd = request.POST['pas']

        check = authenticate(username=un,password=pwd)
        if check:
            if check.is_superuser:
                login(request,check)
                return HttpResponseRedirect('/admin')
            if check.is_staff:
                login(request,check)
                res = HttpResponseRedirect('/dashboard')
                res.set_cookie('userid',check.id)
                return res
        else:
            return render(request,'signin.html',{'status':'Invalid Login details!!'})
    return render(request,'signin.html')

def signout(r):
    logout(r)
    rs = HttpResponseRedirect('/')
    rs.delete_cookie('userid')
    return rs

def dashboard(r):
    usr = registration.objects.get(user__id=r.user.id)
    return render(r,'dashboard.html',{'profile':usr})