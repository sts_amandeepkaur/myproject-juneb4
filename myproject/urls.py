
from django.contrib import admin
from django.urls import path, include
from myapp import views

from django.conf import settings
from django.conf.urls.static import static

from rest_framework import serializers, routers,viewsets
from myapp.models import Product ,category

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id','name','cat','m_price','s_price','image','description','added_on']

class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    class Meta:
        model = category
        fields = ['id','c_name','products']


class ProductViewset(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class catViewset(viewsets.ModelViewSet):
    queryset = category.objects.all()
    serializer_class = CategorySerializer

router = routers.DefaultRouter()
router.register('products',ProductViewset)
router.register('categories',catViewset)
urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/',views.indexpage,name="index"),
    path('about/',views.aboutpage,name="about"),
    path('signup/',views.signup,name="sign"),
    path('checkUserExist/',views.checkUserExist,name="check"),
    path('',views.signin,name="signin"),
    path('signout/',views.signout,name="signout"),
    path('dashboard/',views.dashboard,name="dashboard"),
    path('api/',include(router.urls)),
    path('api-auth/',include('rest_framework.urls'),name="rest_framework")
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
