from django.db import models

class Student(models.Model):
    sub = (
        ('Mr','Mr.'),('Miss','Miss'),('Mrs.','Mrs.'),
    )
    subtitle = models.CharField(max_length=100,choices=sub)
    name = models.CharField(max_length=250)
    reg_no=models.IntegerField(unique=True)
    email = models.EmailField()
    website = models.URLField(blank=True)
    fee = models.DecimalField(max_digits=10,decimal_places=2)
    dob = models.DateField()
    registred_on = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=True)
    def __str__(self):
        return str(self.reg_no)+self.name

class category(models.Model):
    c_name = models.CharField(max_length=250)
    def __str__(self):
        return self.c_name

class Product(models.Model):
    name = models.CharField(max_length=300)
    cat = models.ForeignKey(category,on_delete=models.CASCADE,related_name="products")
    m_price = models.DecimalField(max_digits=10,decimal_places=2)
    s_price = models.DecimalField(max_digits=10,decimal_places=2)
    image = models.ImageField(upload_to="products/%Y/%m/%d")
    description = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.name
from django.contrib.auth.models import User
class registration(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    profile_pic = models.ImageField(upload_to='profiles/%Y/%m/%d')

    def __str__(self):
        return self.user.username


